# excursion_active_learning
excursion_active_learning is an active learning based tool that uses Gaussian Processes to find exclusion contours in high dimensional phase spaces.

Forked from: https://github.com/diana-hep/excursion



#  activeLearningInterface.py  
An interface to use with the 'excursion' active learning algorithm. Presume we have an objective function f(x) that maps R^n to R, and we want to find the contour C such that x $/in$ C <=> f(x)=0. The 'excursion' algorithm helps us to determine the contour C by modelling f(x) as a Gaussian process and prediction points x_i that are likely to close to the contour C based on previously explored points x_j and their objective function's values f(x_j). 

To run the script, you need to prepare two input files via command line options to activeLearningInterface.py:
**--boundsYAML** and **--evaluatedPoints**

**--boundsYAML** <exampleBounds.yaml>
> We need to pass to the activeLearningInterface.py a list of named parameters and bounds for each for the active learning to work. Do this via the --boundsYAML command line options. The content of the file needs to take the shape of:
parameterBounds:
  parameter1: [1,10]
  parameter2: [1e4,1e6]
  parameter3: [-1,1]

See the exampleBounds.yaml file for an example.
  
**--evaluatedPoints** <exampleEvaluatedPoints.csv>
>In addition to the bounds of the parameters in our model, we also need to pass the parameter value points that we have also observed, as well as the measured value of the objective function. E.g. the log( observedXS / theoryXS). We do this via a .csv file. The first one of the file has to be a header line, listing the names of the parameters and the name of the objective function. The parameter names must match the names defined the YAML file passed via --boundsYAML. The other lines are list the values of parameters and objective functions explored in previous iterations
example:
parameter1, parameter2, parameter3, objectiveFunctionValue
2, 2e5, 0.5, 0.1
9, 7e5, -0.5, 0.4
5, 3e4, 0.01, 0.001

See exampleEvaluatedPoints.csv for an example


Other optional values are:

**--outputFileName**
>The next parameter points to explore, as predicted by our Gaussian process model and our acquisition function, are will be saved in a .csv file. This option specifies the name of that .csv file

**--nPoints**
>By default we predict only one next parameter point to explore, but we can also predict more than one, use the --nPoints option to determine how many points we want to have predicted.



Try the interface with the example files:

python activeLearningInterface.py --boundsYAML exampleBounds.yaml  --evaluatedPoints exampleEvaluatedPoints.csv