from excursion import *
from excursion.optimizer.builders import *
import torch
import numpy as np

if __name__ == '__main__':



    #Parameters for the 'ExcursionProblem' object


    # target value for the optimization problem
    # if we have observed cross section (XS) values, and theoretical predictions of the cross section values
    # then the exclusion contour is at thresholds = 0 = log(observed_XS/theory_XS )
    thresholds = [0] 

    # bounds of the parameter phase space in which we want to calculate the exclusion contours
    bounding_box = [[0, 10]] 

    # dimensionality of the parameter phase space in which we want to calculate the exclusion contours 
    ndim = len(bounding_box) 
    
    # step size in each dimension of the phase space (used for what exactly?)
    grid_step_size = [100] 
    
    # I believe this in case we want to show how well we can approximate an explicitly given funtion. 
    functions = [lambda x: x] 

    # 'ExcursionProblem', necessary as input for the 'Optimizer' object, 
    # that does the actual optimization / finding of the contour
    problem_details = ExcursionProblem(thresholds=thresholds, bounding_box=bounding_box, 
        ndim=ndim, grid_step_size=grid_step_size, functions=functions) # init_n_points=None

    problem_details_noTruth = ExcursionProblem(thresholds=thresholds, bounding_box=bounding_box, 
        ndim=ndim, grid_step_size=grid_step_size,functions=functions,init_n_points=0) 
        # specify init_n_points=0 if we do not want the model to pick some initial, randomly selected, random variables




    file = open('algorithm_specs_test.yaml', "r")
    import yaml
    algorithm_opts = yaml.safe_load(file)




    # Here is the 1D examples of the learner ask-and-tell API.
    learner_one = Learner(problem_details, algorithm_opts)

    # even if we don't want to use an unerlying truth model, we still need to use an excursion proble, 
    # with an evauluatable 'truth' function.
    learner_one_noTruth = Learner(problem_details_noTruth, algorithm_opts)




    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    for point in[ 2.,5.,8.] : # tell the model the location and value of the explored points

        x = np.array([point]).reshape(1,1) ; y = np.array([point**2]).reshape(1,1)
        result = learner_one_noTruth.tell(x,y)


    plot(learner_one_noTruth.optimizer.result) # show the model given the x-y values that we told it


    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    #  ask for the next point to explore
    # (currently can only ask for a single next point)
    nextPointToEvaluate= learner_one.ask(learner_one_noTruth)
    print(nextPointToEvaluate)

    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    # tell it the result of the next point to expore, without using the 'truth model
    # that is technically defined within the problem definition
    result = learner_one_noTruth.tell(nextPointToEvaluate,nextPointToEvaluate**2)





    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here



    plot(learner_one_noTruth.optimizer.result)

    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    ## plot the current result:
    # plot(learner_one.optimizer.result)


    result = learner_one.optimizer.result


    # ask for the next point to explore:
    nextPointToEvaluate= learner_one.ask()


    ## evaluate surrogate model at 'x'
    # learner_one.evaluate(x)
    learner_one.evaluate(nextPointToEvaluate) # actually this seems to evaluate the 'true' model


    # result.mean[-1] # get mean of prediction
    # result.cov[-1] # get std deviations of prediction


    nextPointToEvaluate= learner_one.ask( np.array(2))



    nextTrueValue = learner_one.evaluate_and_tell(nextPointToEvaluate)


    result = learner_one.optimizer.get_result() # """Returns the most recent result as a new object """

    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here



    device_opt = ['cuda', 'cpu', 'skcpu']
    dtype_opt = [torch.float64, np.float64]
    dtype_str = 'float64'
    n_initial_points = 3
    jump_start_opt = [True, False]
    model_type_opt = ['ExactGP', 'GridGP', 'SKLearnGP']
    model_fit_opt = ['Adam', 'LBFGS']
    likelihood_options = [0.0, 0.2]

    # Defines the function to minimize over the posterior distribution. Can be either
    #"MES" maximum entropy search
    #"PES" predictive entropy search
    acq_opt = ['pes', 'mes']


    dtype = dtype_opt[1]
    device_str = device_opt[2]
    device = torch.device(device_str) if device_str != 'skcpu' else device_str
    jump_start = jump_start_opt[0]
    model_type = model_type_opt[2]
    fit_optimizer = model_fit_opt[0]
    acq_type = acq_opt[0]

    base_model_kwargs = {}
    base_model_kwargs['device'] = device
    base_model_kwargs['dtype'] = dtype
    base_model_kwargs['likelihood_type'] = 'GaussianLikelihood'
    base_model_kwargs['epsilon'] = likelihood_options[0]





    if jump_start:
        plus_iterations = 0
    else:
        plus_iterations = n_initial_points

    result_length = 0
    n_iterations = 15

    
    base_model = build_model(
        model_type, rangedef=problem_details.rangedef, **base_model_kwargs
    )



    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here





    acq_func = build_acquisition_func(acq_type, device_opt=device, dtype=dtype)

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    optimizer = Optimizer(
        problem_details=problem_details,
        base_model=base_model,
        acq_func=acq_func,
        jump_start=jump_start,
        device=device_str,
        n_initial_points=n_initial_points,
        initial_point_generator="random",
        fit_optimizer=fit_optimizer,
        base_model_kwargs={},
        dtype=dtype_str,
        log=False,
    )


    #parsed yaml outputs:
    #('example', 'Simple Learner Usage')
    #('ninit', 3)
    #('init_type', 'random')
    #('likelihood', {'likelihood_type': 'GaussianLikelihood', 'epsilon': 0.0})
    #('model', {'type': 'SKLearnGP', 'fit_optimizer': 'Adam'})
    #('acq', {'acq_type': 'pes'})
    #('jump_start', True)
    #('device', 'skcpu')
    #('dtype', 'float64')





    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
