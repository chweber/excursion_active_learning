# python activeLearningInterface.py --boundsYAML exampleBounds.yaml  --evaluatedPoints exampleEvaluatedPoints.csv
# python activeLearningInterface.py --boundsYAML exampleBounds.yaml  --evaluatedPoints exampleEvaluatedPoints.csv --nPoints 2 --outputFileName nextPoints.csv



from excursion import *
from excursion.optimizer.builders import *
import torch
import numpy as np
import yaml
import csv
import argparse # to parse command line options
import os
import random



def prepBoundingList(yamlfile, parameterBoundsKey = "parameterBounds"):
    # we need to what are the bounds for each parameter
    # use this function to extract them from a yaml file

    file = open(yamlfile, "r")
    algorithm_opts = yaml.safe_load(file)

    parmeterBoundsDict = algorithm_opts[parameterBoundsKey]

    parameterNamesList = []
    boundsList = []

    for parameterName in parmeterBoundsDict.keys():

        parameterName=parameterName.strip()

        parameterNamesList.append(parameterName)

        #do this in case the bounds are not correctly interpretead, e.g. if we have someghing like 1e-3
        bounds = [float(limit) for limit in parmeterBoundsDict[parameterName] ]

        boundsList.append( bounds)

    return parameterNamesList, boundsList


def parse_csv(file_path):
    data = {}

    with open(file_path, 'r') as csv_file:
        reader = csv.reader(csv_file)

        headers = [ header.strip() for header in next(reader)]  # Read the first row as headers

        for header in headers: 
            data[header] = []

        for row in reader:
            for i, value in enumerate(row):
                data[headers[i]].append(float(value))

    return data


def setupActiveLearner(listOfBoundsForParameters, thresholds = [0], grid_step_size = [10] , 
    truthFunction = [lambda x: x[:,0]] , algorithmOptionYAMLFile = 'algorithm_specs_test.yaml'):
    #Parameters for the 'ExcursionProblem' object

    # thresholds: target value for the optimization problem
    # if we have observed cross section (XS) values, and theoretical predictions of the cross section values
    # then the exclusion contour is at thresholds = 0 = log(observed_XS/theory_XS )

    # listOfBoundsForParameters:  bounds of the parameter phase space in which we want to calculate the exclusion contours

    # dimensionality of the parameter phase space in which we want to calculate the exclusion contours 
    ndim = len(listOfBoundsForParameters) 
    
    # grid_step_size: step size in each dimension of the phase space (I believe this is relevant for plotting)
    # if len(grid_step_size) is differnet from one, we will presume it has purposefully set explicitly along each dimension
    if len(grid_step_size) == 1 : grid_step_size *= ndim
    
    # truthFunction: this is in part used to show the true function in plots


    # 'ExcursionProblem', necessary as input for the 'Optimizer' object, 
    # that does the actual optimization / finding of the contour
    problem_details = ExcursionProblem(thresholds=thresholds, bounding_box=listOfBoundsForParameters, 
        ndim=ndim, grid_step_size=grid_step_size, functions=truthFunction) # init_n_points=None

    algorithm_opts_file = open(algorithmOptionYAMLFile, "r")
    algorithm_opts = yaml.safe_load(algorithm_opts_file)

    # Here is the learner ask-and-tell API object
    excursion_learner = Learner(problem_details, algorithm_opts)

    return excursion_learner


def parseEvalutedPointsCSV(csvFileWithEvaluatedPoints, parameterNames):

    if not os.path.exists(csvFileWithEvaluatedPoints): return None, None
    if os.path.getsize(csvFileWithEvaluatedPoints) == 0: return None, None


    # get the location and observed values in the relevant phase space
    observedPointsDict = parse_csv(csvFileWithEvaluatedPoints)


    # observedPointsDict should have entries for all the dependent variables used in the yaml file
    # defining their bounds, and one extra entry for the measured dependent variable
    dependentVarNameList = list(set(observedPointsDict.keys())-set(parameterNames))

    assert len(dependentVarNameList)==1
    dependentVarName = dependentVarNameList[0]

    nObservations = len(observedPointsDict[parameterNames[0]])

    parameterValuesList=[]; dependentVarValuesList=[]

    for obsIndex in range(nObservations): 

        parameterValuesList.append([   observedPointsDict[parameterName][obsIndex] for parameterName in parameterNames])
        dependentVarValuesList.append( observedPointsDict[dependentVarName][obsIndex])

    return np.array(parameterValuesList), np.array(dependentVarValuesList)


def writeOutNextPoints(nextPointsToExplore, outputFileName = "nextPoints.csv", paramaterNames = None):

    outputFile = open(outputFileName, "w")

    if paramaterNames is not None: outputFile.write(",".join(paramaterNames) + "\n" )

    for pointNr, setOfPoints in enumerate(nextPointsToExplore):  

        stringList = [str(paramVal) for paramVal in setOfPoints]
        outputFile.write(",".join(stringList) )

        if pointNr < len(nextPointsToExplore)-1: 
            outputFile.write("\n")

    outputFile.close()
    
    return None

def getRandomInitialPoint(nPoints, listOfParameterLimits , seed = 1234567):

    random.seed(10)

    pointsToExplore = []

    for x in range(nPoints):

        pointCoordinate = [random.uniform(parameterMin, parameterMax) for parameterMin, parameterMax in listOfParameterLimits ]

        pointsToExplore.append(pointCoordinate)   

    return pointsToExplore


def prepOutputArray(parameterValuesArray, measurementArray, nextPointToExplore ):

    nextPointToExploreArray = np.asarray(nextPointToExplore)

    nPointsToExplore = np.shape(nextPointToExploreArray)[0]

    valueOfNewPoints = np.empty((nPointsToExplore,1))

    valueOfNewPoints[:]=np.nan


    nextPointToExploreWValuesArray = np.concatenate( (nextPointToExploreArray, valueOfNewPoints), axis=1)

    # if we don't have any previous explored points and their values, then we just return the next points
    if parameterValuesArray is None: return nextPointToExploreWValuesArray
    # otherwise we concatenate the prevecously explored ones with the next to be explored points

    measurementArrayReshaped = np.reshape(measurementArray, [len(measurementArray),1] )

    priorPoints = np.concatenate( [parameterValuesArray, measurementArrayReshaped], axis=1)

    return np.concatenate( ( priorPoints, nextPointToExploreWValuesArray), axis=0)

def initilizeArgParser( parser = argparse.ArgumentParser(
    description="An interface to use with the 'excursion' active learning alorithm. \
    Presume we have an objective function f(x) that maps R^n to R, \
    and we want to find the countour C such that x /in C <=>  f(x)=0. \
    The 'excurion' algorithm helps us to determine the contour C \
    by modelling f(x) as a Gaussian process and prediction points x_i \
    that are likely to close to the countour C based on previously \
    explored points x_j and their objective function's values f(x_j).\
    Provided a set of parameters and their their boinds as a YAML file and\
    a csv file with the previously explored values x_j and their objective function's value f(x_j).") ):

    parser.add_argument("--boundsYAML", type=str, required = True, help="\
        YAML file listing the parameters of the problem and their bounds.\
        Parameter names must match the ones listed in the .csv file specified in the '--evaluatedPoints' option\
        Takes the form \
        \
        parameterBounds:\
          parameter1: [1,10]\
          parameter2: [1e4,1e6]\
          parameter3: [-1,1]\
        ")

    parser.add_argument("--evaluatedPoints", type=str, required = True, help="\
        .csv file listing the parameter names and the values previous explored, \
        as well as the value of the objective function for that set of parameters.\
        First line of the .csv file must be a header with the parameter names, \
        matching the ones in the YAML file specified in '--boundsYAML'.\
        Takes the form for example:\
        parameter1, parameter2, parameter3, objectiveFunctionValue\
        2, 2e5, 0.5, 0.1\
        9, 7e5, -0.5, 0.4\
        5, 3e4, 0.01, 0.001\
        ")

    parser.add_argument("--outputFileName", type=str, default="nextPoints.csv" , required = False,
        help="The next point of parameter space as suggested by the 'excursion' package will be saved in a .csv file.\
        This option specifies the name of that .csv file")

    parser.add_argument("--nPoints", "-n", type=int, default = 1 , required = False,
        help="Specify the number of next points we want to predict. Defaults to predicting 1 next point.")

    return parser.parse_args()




if __name__ == '__main__':

    args = initilizeArgParser()

    parameterNames, listOfParameterLimits  = prepBoundingList( args.boundsYAML , parameterBoundsKey = "parameterBounds")


    # we use the parameter names here to ensure that their bounds are later correctly matched to their probed valies

    parameterValuesArray, measurementArray = parseEvalutedPointsCSV(args.evaluatedPoints ,parameterNames)

    if parameterValuesArray is None: nextPointToExplore =  getRandomInitialPoint(args.nPoints, listOfParameterLimits , seed = 1234567)

    else:

        excursion_learner = setupActiveLearner(listOfParameterLimits)

        # tell the active learning optive about the parameter values we have already explored 
        # and the function values we got for there
        result = excursion_learner.tell(parameterValuesArray, measurementArray, fit=True)
        
        # plot(excursion_learner.optimizer.result) # show the model given the x-y values that we told it

        nextPointToExplore = excursion_learner.ask(npoints= args.nPoints)

        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here


    outputArray = prepOutputArray(parameterValuesArray, measurementArray, nextPointToExplore )

    if len(parameterNames) < np.shape(outputArray)[1]: parameterNames.append("dependend_variable")


    writeOutNextPoints(outputArray, outputFileName = args.outputFileName, paramaterNames = parameterNames )

    print("Next point to explore:")
    print( nextPointToExplore)

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
