import numpy as np
import argparse # to parse command line options
import csv


def concatenateInputs(mH,f1,f2):


    if (f1 is None) and (f2 is None): return mH

    if len(np.shape(mH)) == 1: mH = np.reshape(mH, (len(mH),1))
    #np.concatenate((mH,f1),1)

    if f1 is not None: f1 = np.asarray(f1)
    if f2 is not None: f2 = np.asarray(f2)

    if f1 is not None and len(np.shape(f1)) ==0 : f1 = np.asarray([f1])
    if f2 is not None and len(np.shape(f2)) ==0 : f2 = np.asarray([f2])

    if f1 is not None and len(f1)==1: f1=np.full(np.shape(mH),f1)
    if f2 is not None and len(f2)==1: f2=np.full(np.shape(mH),f2)

    if f1 is not None: mH = np.concatenate((mH,f1),1)
    if f2 is not None: mH = np.concatenate((mH,f2),1)

    return mH

def theory(mH,f1=None,f2=None):  

    X = concatenateInputs(mH,f1,f2)

    alpha = np.sqrt(np.square(X[:,1]) + np.square(X[:,2]))

    return np.exp(-1*X[:,0]+alpha)

def observed(mH,f1=None,f2=None):

    X = concatenateInputs(mH,f1,f2)
    #alpha = np.sqrt(np.square(f1) + np.square(f2))
    return np.exp(-0.5*X[:,0])
    #return 1/(mH+2)


def logObservedTheoryRatio1D(X): 
    X = concatenateInputs(X,1.,1.)
    return np.log(observed(X)/theory(X))



def logObservedTheoryRatio2D(X): 
    X = concatenateInputs(X,None,1.)
    return np.log(observed(X)/theory(X))




def logObservedTheoryRatio3D(X): return np.log(observed(X)/theory(X))


def parse_csv(file_path):
    data = {}

    with open(file_path, 'r') as csv_file:
        reader = csv.reader(csv_file)
    
        headers = [ header.strip() for header in next(reader)]  # Read the first row as headers

        for header in headers: 
            data[header] = []

        for row in reader:
            for i, value in enumerate(row):
                data[headers[i]].append(float(value))

    return data


def parseEvalutedPointsCSV(csvFileWithEvaluatedPoints):

    # get the location and observed values in the relevant phase space
    observedPointsDict = parse_csv(csvFileWithEvaluatedPoints)


    parameterNames = observedPointsDict.keys()


    # observedPointsDict should have entries for all the dependent variables used in the yaml file
    # defining their bounds, and one extra entry for the measured dependent variable

    nNewPoints = len(list(observedPointsDict.values())[0])

    parameterValuesList=[]

    for obsIndex in range(nNewPoints): 

        parameterValuesList.append([   observedPointsDict[parameterName][obsIndex] for parameterName in parameterNames])

    return np.array(parameterValuesList), parameterNames


def writeOutNextPoints(phaseSpacePoints, outputFileName = "nextPoints.csv", paramaterNames = None):

    outputFile = open(outputFileName, "w")

    if paramaterNames is not None: outputFile.write(",".join(paramaterNames) + "\n" )

    for pointNr, setOfPoints in enumerate(phaseSpacePoints):  

        stringList = [str(paramVal) for paramVal in setOfPoints]
        outputFile.write(",".join(stringList) )

        if pointNr < len(phaseSpacePoints)-1: 
            outputFile.write("\n")

    outputFile.close()
    
    return None

def selectPointsToCalculate(parsedCSV):

    nRows, nCols = np.shape(parsedCSV)

    nPointsToCalculate = np.sum(np.isnan(parsedCSV))

    pointsToCalculate = []
    pointsAlreadyCalculated = []

    for rowIdx in range(0, nRows):

        calculatePoint = np.isnan(parsedCSV[rowIdx][nCols-1])

        if calculatePoint : pointsToCalculate.append(parsedCSV[rowIdx])
        else:               pointsAlreadyCalculated.append(parsedCSV[rowIdx])


    pointsToCalculateArray = np.asarray(pointsToCalculate)

    if len(pointsAlreadyCalculated) == 0: pointsAlreadyCalculatedArray = np.zeros([0,nCols])
    else:   pointsAlreadyCalculatedArray = np.asarray(pointsAlreadyCalculated)

    return  pointsToCalculateArray , pointsAlreadyCalculatedArray

def runTruthCalculation(inputArray, nDimensions):

    if   nDimensions == 3: return logObservedTheoryRatio3D(inputArray)
    elif nDimensions == 2: return logObservedTheoryRatio2D(inputArray)
    elif nDimensions == 1: return logObservedTheoryRatio1D(inputArray)

    return None


def initilizeArgParser( parser = argparse.ArgumentParser() ):

    parser.add_argument("--numericalInput", type=int, nargs='*', default = [], required =  False, help="\
        List of numbers that we will interpret together with the 'nDimensions' arguments \
        as a list of list, and will use that to run the \
        logObservedTheoryRatio3D, logObservedTheoryRatio3D, or logObservedTheoryRatio3D \
        funcion.")

    parser.add_argument("--nDimensions", type=int, required = False, default= 3,
        choices = [1,2,3],
        help="dimensionality of the problem we want.")



    parser.add_argument("--inputCSV", type=str, default = None, required =  False, help="\
        Will parse this csv file to calculate new points.\
        ")


    parser.add_argument("--outputFileName", type=str, default = "evaluatedPoints.csv", 
        required =  False, help="Name of the output file.\
        ")



    return parser.parse_args()


true_function_1D = [logObservedTheoryRatio1D]
true_function_2D = [logObservedTheoryRatio2D]
true_function_3D = [logObservedTheoryRatio3D]


if __name__ == '__main__':

    args = initilizeArgParser()

    if len(args.numericalInput) > 0:


        assert len(args.input)%args.nDimensions == 0

        proxyInput = [ args.input[ indx:indx+args.nDimensions] for indx in range(0,len(args.input),args.nDimensions) ]

        proxyInputArray = np.array(proxyInput)

        output = runTruthCalculation(proxyInputArray,nDimensions)


    if args.inputCSV is not None:


        parsedCSV, parameterNames = parseEvalutedPointsCSV(args.inputCSV)


        pointsToCalculate , calculatedPoints = selectPointsToCalculate(parsedCSV)

        output = runTruthCalculation(pointsToCalculate[:,0:-1], len(parameterNames)-1)

        # replace NaN's with calculated values
        pointsToCalculate[:,-1] = output

        exploredParameterSpace = np.concatenate((calculatedPoints,pointsToCalculate),axis=0)

        writeOutNextPoints(exploredParameterSpace, outputFileName = args.outputFileName, paramaterNames = parameterNames )



    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

#def theory(X):  
#    # mH = X[:,0]
#    # f1 = X[:,1]
#    # f2 = X[:,2]
#    alpha = np.sqrt(np.square(X[:,1]) + np.square(X[:,2]))
#
#    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
#
#    return np.exp(-1*X[:,0]+alpha)

#def observed(mH,f1,f2):
#    #alpha = np.sqrt(np.square(f1) + np.square(f2))
#    return np.exp(-0.5*mH)
#    #return 1/(mH+2)


#import pdb; pdb.set_trace() # import the debugger and instruct it to stop here