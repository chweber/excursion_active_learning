FROM python:3.8.10-slim AS pythonContainer

# install the python components we want and need
RUN pip install simplejson pyyaml excursion scikit-image scikit-learn==1.1.1 matplotlib torch numpy==1.22.4


ARG PROJECT=workdir

#ARG CACHEBUST=1 # with this the command below will run without cache
COPY . /$PROJECT/excursion_active_learning/


# install the python components we want and need
#RUN pip install simplejson pyyaml excursion scikit-image matplotlib torch

# install git
#RUN apt-get update -y; apt-get install git -y


# clone the excusion package
#RUN git clone https://github.com/diana-hep/excursion.git



## we want the container to start in a bash shell
CMD bash


# docker build -f dockerfile -t christiantmweber/excursion_container:test01 .
# docker push christiantmweber/standardhypotestinverter_for_idds:ROOT_6.24.06-centos7
